#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include "elem.h"

Dlist::Dlist(int x, int y, int w, int h, int oh)
    : Elem(x,y,w,h), Lenght(1), OH(oh), selected(false), scroll(1) {
    Data = new std::string[Lenght];
    Data[0] = "0";
}

void Dlist::draw() const {
    #if SDL > 0
    boxColor(screen, X, Y+H, X+W, Y+H+OH, 0x000000ff);

    roundedBoxColor(screen, X, Y, X+W-H, Y+H, 5, 0x000080ff);
    roundedRectangleColor(screen, X, Y, X+W-H, Y+H, 5, 0xffffffff);
    drawText(Data[0], fontb, 0xffffff, X+2, Y+2); //16

    roundedBoxColor(screen, X+W-H, Y, X+W, Y+H, 5, 0x000080ff);
    roundedRectangleColor(screen, X+W-H, Y, X+W, Y+H, 5, 0xffffffff);
    drawText("v", fontb, 0xffffff, X+W-H/2-4, Y+(H/2)-9); //16

    if(selected) {

        for(int i=1; i<OH/20+1 && i<Lenght; i++)
         {
            if(Data[i+scroll-1]==Data[0]) roundedBoxColor(screen, X, Y+H+(i-1)*20, X+W-H, Y+H+i*20, 5, 0x0000ffff);
            else roundedBoxColor(screen, X, Y+H+(i-1)*20, X+W-H, Y+H+i*20, 5, 0x000080ff);
            roundedRectangleColor(screen, X, Y+H+(i-1)*20, X+W-H, Y+H+i*20, 5, 0xffffffff);
            drawText(Data[i+scroll-1], fontb, 0xffffff, X+2, Y+H+(i-1)*20); //16
         }
         if((OH/20)<Lenght)
         {
             roundedRectangleColor(screen, X+W-H+2, Y+4+H+(scroll-1)*Lenght, X+W-2, Y+H+(scroll-1)*Lenght+(OH/20)*OH/(Lenght), 5, 0xffffffff);
         }
    }
    #endif
}

void Dlist::handle() {
    #if SDL > 0
    if(ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT && is_selected() && ev.button.x>X+W-H ) {
        selected = !selected;

    }
    if(selected) {
        if(ev.button.button==SDL_BUTTON_WHEELUP && scroll>1) scroll--;
        if(ev.button.button==SDL_BUTTON_WHEELDOWN && scroll<Lenght-OH/20 && (OH/20)<Lenght) scroll++;
        for(int i = 0; ev.button.button == SDL_BUTTON_LEFT && ev.button.x>X && ev.button.y>Y+H && ev.button.x<X+W && i<OH/20+1 && i<Lenght; i++) {
            if(ev.button.y<Y+H+20*i && ev.button.y>Y+H+20*i-20)Data[0]=Data[i+scroll-1];
            selected = false;
        }
    }
    #endif
}


bool Dlist::is_selected() {
    #if SDL > 0
    if(selected) {
        if(ev.button.x>X && ev.button.x<X+W && ev.button.y>Y && ev.button.y<Y+H+OH) {
            return true;//ha lenyílik a lista akkor az Elem maradjon focus-ba, akkor is ha a listára kattintunk
        } else selected = false;

    }
    return ev.button.x>X && ev.button.x<X+W && ev.button.y>Y && ev.button.y<Y+H;
    #endif
    #if SDL <= 0
    return false;
    #endif // SDL
}

void Dlist::operator<<(const std::string& data) {
    std::string * nData = new std::string[Lenght + 1];
    for(int i = 0; i < Lenght; i++) nData[i] = Data[i];
    nData[Lenght] = data;
    Lenght ++;
    delete[] Data;
    Data = nData;
}
void Dlist::operator>>(std::string& data) {
    data = Data[0];
}

Dlist::~Dlist() {
    delete[] Data;
}
