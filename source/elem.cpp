#include "elem.h"
#include "global.h"

Elem::Elem(){}

Elem::Elem(const int &x, const int &y, const int &w, const int &h): X(x), Y(y), W(w), H(h) {}

bool Elem::is_selected() const{
    #if SDL > 0
    return ev.button.x>X && ev.button.x<X+W && ev.button.y>Y && ev.button.y<Y+H;
    #endif
    #if SDL <= 0
    return false;
    #endif // SDL
}

void Elem::draw() const {}
void Elem::handle() {}
void Elem::operator<<(const std::string&) {}
void Elem::operator>>(std::string&) {}
void Elem::operator<<(const bool&){}
void Elem::operator>>(bool&){}
bool Elem::pressed(){return false;}
Elem::~Elem(){}
