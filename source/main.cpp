#include "global.h"
///
///Az sdl v�lltoz� a global.h file ban �ll�that�
///SDL 0 eset�n sz�veges m�dban fut
///
#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include <string>
#include <iostream>
#include <sstream>

#include "elem.h"
#include "data.h"
#include "global.h"

#if SDL > 0
SDL_Event ev;
SDL_Surface *screen;
TTF_Font *fonta, *fontb;
#endif


int main(int argc, char **argv) {
    #if SDL > 0
    //SDL inicializ�l�sa �s ablak megnyit�sa
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
    screen = SDL_SetVideoMode(1280, 720, 0, SDL_ANYFORMAT);
    SDL_WM_SetCaption("Map editor", "Map editor");

    TTF_Init();
    fonta = TTF_OpenFont("LiberationSerif-Regular.ttf", 32);
    fontb = TTF_OpenFont("LiberationSerif-Regular.ttf", 16);
    bool quit = false;

    bool tmp;
    int rooms = 0;
    int room = 0;
    int x = 0;
    int y = 0;
    std::string stmp;

    int focus = -1;
    size_t lenght = 9;
    Elem **elements = new Elem*[lenght];
    elements[0] = new Button(10, 10, 100, 20, "Map");
    elements[1] = new Button(110, 10, 100, 20, "Warp");
    elements[2] = new Button(210, 10, 100, 20, "Interract");
    elements[3] = new Button(10, 40, 60, 20, "Add");
    elements[4] = new Button(70, 40, 60, 20, "Remove");
    elements[5] = new TextInput(140, 40, 300, 20);
    elements[6] = new Dlist(550, 10, 100, 20, 180);
    elements[7] = new Button(660, 10, 20, 20, "+");
    elements[8] = new Button(660, 30, 20, 20, "-");

    *elements[3] << true;
    *elements[0] << true;
    *elements[5] << std::string("room.png");
    *elements[6] << std::string("0");

    DatA datA(500, 230);
    datA.addR(std::string("room.png"), 0);

    int focusM = -1;
    size_t lenghtM = 3;
    Elem **elementsM = new Elem*[lenght];
    elementsM[0] = new Button(10, 70, 60, 20, "S");
    elementsM[1] = new Button(70, 70, 60, 20, "T");
    elementsM[2] = new Select(10, 150, 464, 528);

    *elementsM[0] << true;

    while(!quit) {
        SDL_WaitEvent(&ev);
        boxColor(screen, 0, 0, 1280, 720, 0x000000ff);
        if(ev.type == SDL_QUIT) quit = true;

        drawText( "Background:", fontb, 0xffffff, 320, 20);
        drawText( "Room:", fontb, 0xffffff, 500, 10);

        datA.draw(room);
        if (ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT && datA.is_selected()){
            std::stringstream ss, cs;
            char c;
            *elements[6] >> stmp;
            ss << stmp;
            ss >> room;
            *elementsM[2] >> stmp;
            cs << stmp;
            cs >> x >> c >> y;
            datA.addM(room, x, y);
        }

        if (ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT) {
            for (size_t i=0; i<lenght; i++) {
                if (elements[i]->is_selected()) {
                    focus = i;
                }
            }
        }
        if (focus!=-1) {
            elements[focus]->handle();
        }
        for (size_t i=0; i < lenght; i++) {
            elements[i]->draw();
        }

        //////////////////
        if(elements[0]->pressed()) {
            *elements[0] << true;
            *elements[1] << false;
            *elements[2] << false;
        }
        if(elements[1]->pressed()) {
            *elements[0] << false;
            *elements[1] << true;
            *elements[2] << false;
        }
        if(elements[2]->pressed()) {
            *elements[0] << false;
            *elements[1] << false;
            *elements[2] << true;
        }
        //////////////////
        if(elements[3]->pressed()) {
            *elements[3] << true;
            *elements[4] << false;
        }
        if(elements[4]->pressed()) {
            *elements[3] << false;
            *elements[4] << true;
        }

        ////////+
        if(elements[7]->pressed()) {
            rooms++;
            std::stringstream ss;
            ss << rooms;
            *elements[6] << ss.str();
            *elements[5] >> stmp;
            datA.addR(stmp, rooms);
        }

        ///map
        *elements[0] >> tmp;
        if(tmp) {
            if (ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT) {
                for (size_t i=0; i<lenghtM; i++) {
                    if (elementsM[i]->is_selected()) {
                        focusM = i;
                    }
                }
            }
            if (focusM!=-1) {
                elementsM[focusM]->handle();
            }
            for (size_t i=0; i < lenghtM; i++) {
                elementsM[i]->draw();
            }

            if(elementsM[0]->pressed()) {
                *elementsM[0] << true;
                *elementsM[1] << false;
            }
            if(elementsM[1]->pressed()) {
                *elementsM[0] << false;
                *elementsM[1] << true;
            }
        }
        *elementsM[2]>>stmp;
        drawText(stmp,fonta,0xffffff, 10, 680);

        SDL_Flip(screen);
    }
    for(size_t  i=0; i< lenght; i++) delete elements[i];
    delete[] elements;
    for(size_t  i=0; i< lenghtM; i++) delete elementsM[i];
    delete[] elements;
#endif

    #if SDL <= 0
    std::cout<<"ez a programom sz�veges tesztje(amit a jporta bevesz, rendes futtat�shoz sdl sz�ks�ges.\n)";
    int error = 0;
    bool tmp;
    std::string stmp;
    size_t lenght = 4;
    Elem **elements = new Elem*[lenght];
    elements[0] = new Button(10, 10, 100, 20, "Test");
    elements[1] = new TextInput(140, 40, 300, 20);
    elements[2] = new Dlist(550, 10, 100, 20, 180);
    elements[3] = new Select(10, 150, 464, 528);

    std::cout<<"button extractor operator tesztje:\n";
    *elements[0] >> tmp;
    if(!tmp) std::cout<<"Sikeres!\n";
    else error++;

    std::cout<<"button inserter operator tesztje:\n";
    tmp = true;
    *elements[0] << tmp;
    *elements[0] >> tmp;
    if(tmp) std::cout<<"Sikeres!\n";
    else error++;

    std::cout<<"Button::pressed() tesztje:\n";
    if(!elements[0]->pressed()) std::cout<<"Sikeres!\n";
    else error++;

    std::cout<<"textinput inserter �s extractor operatorainak tesztje:\n";
    tmp = true;
    *elements[1] << std::string("TEST");
    *elements[1] >> stmp;
    if(stmp == std::string("TEST")) std::cout<<"Sikeres!\n";
    else error++;

    std::cout<<"dlist extractor operator tesztje:\n";
    tmp = true;
    *elements[2] >> stmp;
    if(stmp == std::string("0")) std::cout<<"Sikeres!\n";
    else error++;

    std::cout<<"Select extractor operator tesztje:\n";
    tmp = true;
    *elements[3] >> stmp;
    if(stmp == std::string("0/0")) std::cout<<"Sikeres!\n";
    else error++;

    if(error == 0) std::cout<<"\nMinden teszt sikeres ha a memtrace sem dobott hib�t\n";
    else std::cout<<"\nHib�k sz�ma:"<< error << std::endl;
    #endif // SDL

    for(size_t  i=0; i< lenght; i++) delete elements[i];
    delete[] elements;
    return 0;
}
