#ifndef ELEM
#define ELEM
#include <string>

///Grafikus fel�let egyszer� elemei

class Elem {
protected:
    int X, Y, W, H;
public:
    Elem();
    Elem(const int &x, const int &y, const int &w, const int &h);
    virtual bool is_selected() const;
    virtual void draw() const;
    virtual void handle();
    virtual void operator<<(const std::string&);
    virtual void operator>>(std::string&);
    virtual void operator<<(const bool&);
    virtual void operator>>(bool&);
    virtual bool pressed();
    virtual ~Elem();
};

///Gomb
class Button : public Elem {
    std::string Name; //gomb felirata
    bool State;
    bool Press;

public:
    Button(int x, int y, int w, int h, std::string name);
    void draw() const ;
    void handle();
    virtual void operator<<(const bool&);
    virtual void operator>>(bool&);
    bool pressed();
};

///sz�vegbeviteli mez�
class TextInput : public Elem {
    std::string Data;

public:
    TextInput(int x, int y, int w, int h);
    void draw() const ;
    void handle();
    void operator<<(const std::string&);
    void operator>>(std::string&);
};

///leg�rd�l� men�
class Dlist : public Elem {
    std::string* Data;
    int Lenght;
    int OH; //lista magass�ga
    bool selected; //lista ki van-e nyitva
    int scroll; // g�rget�shez a poz�ci�

public:
    Dlist(int x, int y, int w, int h, int oh);
    void draw() const ;
    void handle();
    virtual void operator<<(const std::string&);
    virtual void operator>>(std::string&);
    bool is_selected();
    ~Dlist();
};

///
class Select : public Elem {
    int MX;
    int MY;
public:
    Select(int x, int y, int w, int h);
    void draw() const ;
    void handle();
    void operator>>(std::string&);

};

#endif // Elem
