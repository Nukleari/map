#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>


//Sz�veg kiir�sa
void drawText(const std::string &text, TTF_Font *font, int color, int x, int y){

    SDL_Surface *texts;
    SDL_Color fcolor;
    SDL_Rect dstrect;

    dstrect.x = x;
    dstrect.y = y;
    dstrect.w = 0;
    dstrect.h = 0;

    //sz�tbontja a sz�n �rt�k�t k�l�n red green blue �rt�kekre
    fcolor.r = (color & 0xFF0000) >> 16;
    fcolor.g = (color & 0x00FF00) >> 8;
    fcolor.b = (color & 0x0000FF);

    //a sz�veget egy ideiglenes SDL surface v�lltoz�ra rendereli �s ezt m�solja majd az ideiglenes v�lltoz�t t�rli.
    texts = TTF_RenderUTF8_Blended(font, text.c_str(), fcolor);
    SDL_BlitSurface(texts, NULL, screen, &dstrect);
    SDL_FreeSurface(texts);
}

void drawTextCenter(const std::string &text, TTF_Font *font, int color, int x, int y){

    SDL_Surface *texts;
    SDL_Color fcolor;
    SDL_Rect dstrect;

    dstrect.x = x;
    dstrect.y = y;
    dstrect.w = 0;
    dstrect.h = 0;

    //sz�tbontja a sz�n �rt�k�t k�l�n red green blue �rt�kekre
    fcolor.r = (color & 0xFF0000) >> 16;
    fcolor.g = (color & 0x00FF00) >> 8;
    fcolor.b = (color & 0x0000FF);

    //a sz�veget egy ideiglenes SDL surface v�lltoz�ra rendereli �s ezt m�solja majd az ideiglenes v�lltoz�t t�rli.
    texts = TTF_RenderUTF8_Blended(font, text.c_str(), fcolor);
    dstrect.x -= texts->w/2;
    SDL_BlitSurface(texts, NULL, screen, &dstrect);
    SDL_FreeSurface(texts);
}

void drawPic(const std::string &file, int srcx, int srcy, int w, int h, int x, int y){

    SDL_Surface *img;
    SDL_Rect srcrect;
    SDL_Rect dstrect;

    srcrect.x = srcx;
    srcrect.y = srcy;
    srcrect.w = w;
    srcrect.h = h;
    dstrect.x = x;
    dstrect.y = y;
    dstrect.w = 0;
    dstrect.h = 0;

    //a k�pet egy ideiglenes SDL surface v�lltoz�ra rendereli �s ezt m�solja majd az ideiglenes v�lltoz�t t�rli.
    img = IMG_Load(file.c_str());
    SDL_BlitSurface(img, &srcrect, screen, &dstrect);
    SDL_FreeSurface(img);
}
#endif
