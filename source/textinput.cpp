#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include <string>
#include "elem.h"

TextInput::TextInput(int x, int y, int w, int h): Elem(x,y,w,h), Data(""){}

void TextInput::draw() const {
    #if SDL > 0
    roundedBoxColor(screen, X, Y, X+W, Y+H, 5, 0x0000ffff);
    roundedBoxColor(screen, X, Y, X+W, Y+H, 5, 0x000080ff);
    roundedRectangleColor(screen, X, Y, X+W, Y+H, 5, 0xffffffff);
    if(H>34)drawText(Data, fonta, 0xffffff, X+2, Y+(H/2)-17); // 32
    else drawText(Data, fontb, 0xffffff, X+2, Y+(H/2)-9); //16
    #endif
}
void TextInput::handle(){
    #if SDL > 0
    if(ev.type == SDL_KEYDOWN){
        if(ev.key.keysym.sym == '\b'){
            if(Data.length() > 0) Data.erase(Data.length() - 1, 1);
        }else{
            Data.insert(Data.length(),1,ev.key.keysym.sym);
        }
    }
    #endif
}
void TextInput::operator<<(const std::string& data){Data = data;}
void TextInput::operator>>(std::string& data){data = Data;}

