#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include <iostream>
#include <sstream>
#include "elem.h"

Select::Select(int x, int y, int w, int h): Elem(x,y,w,h), MX(0), MY(0){}

void Select::draw() const {
    #if SDL > 0
    drawPic("texture_small.png" , 0, 0, W, H, X, Y);
    roundedRectangleColor(screen, X + MX*16, Y + MY*16, 16 + X + MX*16, 16 + Y + MY*16, 2, 0xffffffff);
    #endif
}
void Select::handle(){
    #if SDL > 0
    if(ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT && is_selected()){
        MX = (ev.button.x - X) / 16;
        MY = (ev.button.y - Y) / 16;
    }
    #endif
}
void Select::operator>>(std::string& pos){
    std::stringstream ss;
    ss<< MX << '/' << MY;
    ss >> pos;
}
