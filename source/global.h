#ifndef GLOBAL
#define GLOBAL

#define SDL 1

#include <string>

#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#if SDL > 0
extern SDL_Event ev;
extern SDL_Surface *screen;
extern TTF_Font *fonta, *fontb;

void drawText(const std::string &text, TTF_Font *font, int color, int x, int y);
void drawTextCenter(const std::string &text, TTF_Font *font, int color, int x, int y);
void drawPic(const std::string &file, int srcx, int srcy, int w, int h, int x, int y);
#endif

#endif // GLOBAL
