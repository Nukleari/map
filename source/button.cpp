#include "global.h"

#if SDL > 0
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#endif

#include "elem.h"


Button::Button(int x, int y, int w, int h, std::string name): Elem(x,y,w,h), Name(name), State(false), Press(false){}

void Button::draw() const {

    #if SDL > 0
    if(State) roundedBoxColor(screen, X, Y, X+W, Y+H, 5, 0x0000ffff);
    else roundedBoxColor(screen, X, Y, X+W, Y+H, 5, 0x000080ff);
    roundedRectangleColor(screen, X, Y, X+W, Y+H, 5, 0xffffffff);
    if(H>34)drawText(Name, fonta, 0xffffff, X+24, Y+(H/2)-17); // 32
    else drawTextCenter(Name, fontb, 0xffffff, X+(W/2), Y+(H/2)-9); //16
    #endif


}
void Button::handle(){
    #if SDL > 0
    Press = false;
    if(ev.type == SDL_MOUSEBUTTONDOWN && ev.button.button == SDL_BUTTON_LEFT && is_selected()){
        Press = true;
    }
    #endif
}
void Button::operator<<(const bool& state){State = state;}
void Button::operator>>(bool& state){state = State;}
bool Button::pressed(){return Press;}
