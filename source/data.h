#ifndef DATA
#define DATA
#include <string>
#include "elem.h"
///

class Map{
public:
    std::string Type;
    int X, Y,  pX_dX, pY_dY, W_dR, H_iN;
    Map * next;
public:
    Map();
    Map(const std::string & type,const int & x,const int & y,const int &  px_dx,const int & py_dy,const int & w_dr,const int & h_in);
};

class Room{
public:
    std::string BackG;
    int Number;
    Map * map;
    Room * next;
public:
    Room();
    Room(const std::string & backG,const int & number);

    void add(const std::string & type,const int & x,const int & y,const int &  px_dx,const int & py_dy,const int & w_dr,const int & h_in);
    void rem(Map * ptr);

    ~Room();
};
/*
class Interract{
protected:
    int ID, Room, X, Y;
    std::string ItemN;
    std::string Have;
    std::string DHave;
    std::string ItemG;
    Interract * next;
public:
    Interract(const int & id, const std::string &,const std::string &,const std::string &,const std::string &);
};
*/
class DatA{
    Room * room;
    //Interract * interract;
    int X;
    int Y;
public:
    DatA(const int &x,const int &y);
    void load();
    void save();

    void addR(const std::string & backG, const int & number);
    void remR(const int & number);

    void addM(const int & roomnumber, const int & x, const int & y);
/*
    void addI();
    void remI();
    void editI();
*/
    void draw(const int & number);
    bool is_selected() const;
    ~DatA();
};

#endif // data
